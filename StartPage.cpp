#include "StartPage.h"

int __stdcall WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
    // Initialize the COM Library and enable OLE functionality.
    if (OleInitialize(NULL) < 0)
    {
        MsgBox(L"Could not initialize the COM library");
        return 0;
    }

    // Set up the window class for the window that will host the start page.
    WNDCLASS wcHost = {0};
    wcHost.lpszClassName = L"ShellStartPage";
    wcHost.lpfnWndProc = (WNDPROC)ShellStartPageWndProc;
    wcHost.hCursor = LoadCursorW(NULL, (LPCWSTR)IDC_ARROW);
    wcHost.hbrBackground = (HBRUSH)BACKGROUND_BLUE;
    wcHost.cbWndExtra = 4;
    wcHost.hInstance = GetModuleHandleW(NULL);
    if (!RegisterClass(&wcHost))
    {
        MsgBox(L"Could not create a host window class");
        OleUninitialize();
        return 0;
    }

    // Create the window that will host the start page.
    HWND hwndHost = CreateWindowEx(WS_EX_TOOLWINDOW, wcHost.lpszClassName, NULL, WS_POPUP, 0, 0, 0, 0, NULL, NULL, wcHost.hInstance, NULL);
    if (!hwndHost)
    {
        MsgBox(L"Could not create a host window");
        OleUninitialize();
        return 0;
    }

    // Create the start page object via COM. Failure here indicates the start page class isn't registered.
    CShellStartPage *startPage;
    IID iid;
    CLSID pclsid;
    CLSIDFromString(L"{8DA27716-B6B1-44BE-82B7-2ACFFB37C45F}", &pclsid); // CShellStartPage
    IIDFromString(L"{9AD468E6-00E2-4357-9D01-6FFA43E31BED}", &iid); // IShellStartPage
    HRESULT result = CoCreateInstance(pclsid, NULL, CLSCTX_INPROC_SERVER, iid, (LPVOID *)&startPage);
    if (result < 0)
    {
        MsgBox(L"Could not create a CShellStartPage COM object, is this actually a LH build with a start page?");
        OleUninitialize();
        return 0;
    }

    // We're finally here, launch the start page!
    if (startPage->Create(hwndHost, NULL))
    {
        MsgBox(L"Could not create a start page");
        OleUninitialize();
        return 0;
    }

    // Loop through dispatching any window messages.
    while (true)
    {
        tagMSG msg;
        while (!PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            WaitMessage();
        }

        if (msg.message == WM_QUIT)
        {
            // If the message tells us to quit, time to break out of the loop.
            break;
        }

        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    if (IsWindow(startPage->hWnd))
    {
        // Tell the start page to wrap it up, we're closing.
        SendMessage(startPage->hWnd, WM_ENDSESSION, TRUE, 0);
    }

    // Cleanup the event handles, and unload the COM library.
    OleUninitialize();
    return 0;
}

LRESULT ShellStartPageWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (msg == WM_DESTROY)
    {
        PostQuitMessage(0);
    }
    else
    {
        if (msg != WM_CLOSE)
        {
            return DefWindowProc(hWnd, msg, wParam, lParam);
        }
        DestroyWindow(hWnd);
    }
    return 0;
}

int MsgBox(LPCWSTR lpText)
{
    return MessageBox(NULL, lpText, L"Start Page Launcher", MB_ICONERROR);
}
