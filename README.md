# Start Page Launcher

This project intends to provide a clean, documented implementation of a start page launcher for Longhorn builds 3706 through 4002.

This assumes that the XML is accessible under the current user context at `\\shelltest\scratch\ewad\oobe.xml`.

This is written in C++, with a project file available for Visual Studio .NET (2002).

## Thanks
Thanks to Stanimir Stoyanov for his original binary launcher, and to Lucas Brooks for his buildable [decompilation](https://github.com/lucasbrooks/Longhorn-Start-Page-Launcher).
