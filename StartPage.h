#include <windows.h>

// The Start Page COM type from ShellInterop.dll
class CShellStartPage
{
private:
    // Reference counter, used by the Garbage Collector, since this does use Managed C++
    int refCount;

public:
    // Three methods before Create, which we're interested in.
    // There's also more afterwards, but we don't have to define them to get going.
    // Maybe one day I'll get round to reverse engineering this other stuff.
    virtual void __stdcall unknown_1();
    virtual void __stdcall unknown_2();
    virtual void __stdcall unknown_3();
    
    // Creates a Start Page
    // hwndParent is the parent window that the start page should be created inside.
    // phwnd lets you pass a pointer, which will be filled with the hWnd of the Start Page
    virtual CShellStartPage* __stdcall Create(HWND hwndParent, HWND * phwnd);

    // Pointer to an Avalon Hwnd Dispatcher, just a generic pointer type here since we
    // don't need to use it.
    void* hWndDispatcher;

    // the hWnd of the Start Page
    HWND hWnd;
};

// This processes messages sent to the host window. 
LRESULT ShellStartPageWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

// This just throws a quick and dirty error message box
int MsgBox(LPCWSTR lpText);